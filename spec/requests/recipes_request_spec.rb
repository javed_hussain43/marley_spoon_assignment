require 'rails_helper'

RSpec.describe "Recipes", type: :request do

	describe 'index' do
		
		subject { get root_path }
		
		
		context 'when the page loads all the recipes' do
			
			before { subject }

			it 'should return 200 sucess status code' do
        expect(response).to have_http_status(:success)
			end

			it 'should check the count' do
				expect(response.body).to match("Total 4 Recipes found")
			end

			it 'should render template and partial' do
				expect(response).to render_template(:index)
				expect(response).to render_template('_recipe')
			end
		end

		context 'When Contentful API is down.' do
			before do
				allow_any_instance_of(Contentful::Client).to receive(:entries).and_raise(TimeoutError)
				subject
			end

			it 'should return 200 sucess status code' do
        expect(response).to have_http_status(:success)
			end

			it 'should not check the count' do
				expect(response.body).not_to match("Total 4 Recipes found")
			end
		end
	end

	describe 'show' do
		context 'Recipe id is present in the response' do

			before do
				get recipe_path('5jy9hcMeEgQ4maKGqIOYW6')
			end

			it 'should return 200 sucess status code' do
        expect(response).to have_http_status(:success)
			end

			it 'should contain the text' do
				expect(response.body).to match("Recipe is successfully fetched")
			end

		end

		context 'Recipe id is not present in the response' do

			before do
				get recipe_path(Time.now.to_i)
			end

			it 'should return 200 sucess status code' do
        expect(response).to have_http_status(:success)
			end

			it 'should contain the text' do
				expect(response.body).to match("No Recipe Found!")
			end

		end
	end

end
