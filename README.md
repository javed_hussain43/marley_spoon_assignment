# README

We are providing you with an API key and Space ID to an API for Marley Spoon recipes on Contentful (Content Delivery API). All the information necessary will be available in Contentul's documentation. Your challenge is to write a small application to consume the data from this API and display it.

# Solution

Implemented the solution using contentful API.

# Enhancements

1. Pagination of results

2. We can go for cache to store the information.



# How to Build and Run 

1. Please make sure you have installed ruby verison '2.7.1'

2. Please create a separate gemset and run 'Bundle Install'

3. Run 'rails s' in the terminal.

4. Go to localhost:3000 and start searching.

