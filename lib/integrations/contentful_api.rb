require 'contentful'

module Integrations
	class ContentfulApi
		CONTENT_TYPE   = 'recipe'.freeze
		SUCCESS_STATUS = 'success'.freeze
		FAILURE_STATUS = 'failed'.freeze

		def initialize
			@response = {
					error: [],
					status: nil,
					recipes: []
			}
		end

		def call
			begin
				response[:recipes] = client.entries(content_type: CONTENT_TYPE).entries
				response[:status]  = SUCCESS_STATUS
			rescue TimeoutError
				response[:status]  = FAILURE_STATUS
				response[:error].push({type: 'API TimeOutError', message: 'API timed out!'})
			end
			response
		end

		attr_reader :response


		private

		def client
			Contentful::Client.new(
					space:           space_id,
					access_token:    access_token,
					environment:     environment_id,
					dynamic_entries: :auto
			)
		end

		def space_id
			ENV['SPACEID']
		end

		def environment_id
			ENV['ENVIRONMENTID']
		end

		def access_token
			ENV['ACCESSTOKEN']
		end
	end
end