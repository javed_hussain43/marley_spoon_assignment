class RecipeDecorator
	EMPTY_STRING = ""

	def initialize(object)
		@object = object
		@fields = object.fields.keys
	end

	def title
		key_present?(:title) ?  object.title : EMPTY_STRING
	end

	def image
		key_present?(:photo) ?  object.photo.url : EMPTY_STRING	
	end

	def chef_name
		key_present?(:chef) ? object.chef.name : EMPTY_STRING
	end

	def description
		key_present?(:description) ? object.description : EMPTY_STRING
	end

	def tags
		key_present?(:tags) ? object.tags : []
	end

	attr_reader :object, :fields

	def key_present?(key)
		fields.include? key
	end
	
end