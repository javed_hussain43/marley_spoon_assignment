class RecipesController < ApplicationController
  before_action :load_recipes

  def index
    @recipes = @recipes_services.find_all
    flash[:notice] = "Total #{@recipes.count} Recipes found!"
  end

  def show
    recipe = @recipes_services.find_by_id
    if recipe
      call_decorator(recipe)
      
      flash[:notice] = 'Recipe is successfully fetched'
    else
      flash[:error]  = 'No Recipe Found!'
    end
  end

  private

  def recipes_params
  	params.permit(:id)
  end

  def load_recipes
  	@recipes_services = RecipesServices::LoadRecipes.new(recipes_params)
  end

  def call_decorator(recipe)
    @recipe = RecipeDecorator.new(recipe)
  end

end
