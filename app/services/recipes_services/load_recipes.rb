module RecipesServices
	class LoadRecipes
		SUCCESS_STATUS = 'success'.freeze

		def initialize(params)
			@params  = params
			@response = call_contentful_api
		end

		def find_all
			check_status ? response[:recipes] : []
		end

		def find_by_id
			check_status ? find_recipe : nil
		end

		attr_reader :response, :params

		private

		def check_status
			response[:status] == SUCCESS_STATUS
		end

		def call_contentful_api
			contentful_api = Integrations::ContentfulApi.new
			contentful_api.call
		end

		def find_recipe
			response[:recipes].select do |recipe|
				recipe.id == params[:id]
			end.first
		end
	end
end



